<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Articles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedBigInteger('article_id');
           $table->string('url');
           $table->string('section');
           $table->string('byline');
           $table->string('title');
           $table->string('abstract');
           $table->string('published_date');
           $table->string('source');
           $table->string('media')->nullable();
           $table->string('media_caption')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
