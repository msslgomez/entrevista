<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Dash/Index', [
            'articles' => Article::all()
        ]);
    }

    public function search(Request $request)
    {
        return back()->with('success',
            Article::where('title', 'LIKE', '%' . $request->search . '%')->get()
        );
    }
}
