<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class getArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtener los articulos del New York Times mas recientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return \Illuminate\Support\Collection
     */
    public function handle()
    {
        $response = Http::get('https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=QcPyDKBPJeuALsjrol1IxxLJtAAYMStt');

        $data = $response->json();

        $ids = [];

        try {
            foreach ($data['results'] as $article) {

                if (!empty($article['media'])){
                    $media = $article['media'][0]['media-metadata'];
                    $largest = max(array_column($media, 'width'));
                    $index = array_search($largest, array_column($media, 'width'));
                }

                $art = Article::where('article_id', $article['id'])->first();
                if (empty($art)) {
                    $art = new Article();
                }
                $art->fill($article);
                $art->article_id = $article['id'];
                $art->media = empty($article['media']) ? null : $media[$index]['url'];
                $art->media_caption = empty($article['media']) ? null : $article['media'][0]['caption'];
                $art->save();

                $ids[] = $article['id'];
            }


            Article::whereNotIn('article_id', $ids)->delete();

            $this->info("Articulos Sincronizados Correctamente.");

        } catch (Throwable $e) {
            $this->error($e->getMessage());
        }
    }
}
