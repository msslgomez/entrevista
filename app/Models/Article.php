<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'article_id',
        'url',
        'section',
        'byline',
        'title',
        'abstract',
        'published_date',
        'source',
        'media'
    ];
}
